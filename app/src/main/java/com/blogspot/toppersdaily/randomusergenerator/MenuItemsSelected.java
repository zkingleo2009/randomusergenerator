package com.blogspot.toppersdaily.randomusergenerator;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by zaina on 5/11/2016.
 */
public class MenuItemsSelected {
    public static MenuItemsSelected _Selected = new MenuItemsSelected();
    private MainActivity context;
    private String url = "";


    private MenuItemsSelected() {
    }

    public void setContext(MainActivity context) {
        this.context = context;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void AboutSelected() {

    }

    public String CountrySelected(String paramUrl) {
        url = paramUrl;
        final String[] countriesCode = {"", "AU", "BR", "CA", "DK", "CH", "DE", "ES", "FI", "FR", "GB", "IE", "NL", "NZ", "US", "TR"};
        final CharSequence countries[] = new CharSequence[]{"NONE", "AUSTRALIA", "BRAZIL", "CANADA", "DENMARK", "SWITZERLAND", "GERMANY",
                "SPAIN", "FINLAND", "FRANCE", "UNITED KINGDOM", "IRELAND", "NETHERLANDS", "NEW ZEALAND", "UNITED STATES", "TURKEY"};
        AlertDialog.Builder country = new AlertDialog.Builder(context);
        country.setTitle("SELECT COUNTRY");
        country.setItems(countries, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]
                        switch (which) {
                            case 0:
                                if (url.contains("nat")) {
                                    if (url.contains("?")) {
                                        if (url.contains("gender"))
                                            url = url.replaceAll("&", "");
                                        else
                                            url = url.replace("?", "");

                                        String cs;
                                        for (int i = 1; i < countriesCode.length; i++) {
                                            cs = countriesCode[i];
                                            if (url.contains("nat=" + cs)) {
                                                url = url.replace("nat=" + cs, "");
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                                if (!url.contains("nat=")) {
                                    if (url.contains("?")) {
                                        if (url.contains("gender"))
                                            url += "&";
                                        url += "nat=" + countriesCode[which];
                                    } else
                                        url += "?nat=" + countriesCode[which];
                                } else if (!url.contains("nat=" + countriesCode[which])) {
                                    String cs;
                                    for (int i = 1; i < countriesCode.length; i++) {
                                        cs = countriesCode[i];
                                        if (url.contains("nat=" + cs)) {
                                            url = url.replace("nat=" + cs, "nat=" + countriesCode[which]);
                                            break;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }

        );
        country.show();
        return url;
    }

    public String GenderSelected(String paramUrl) {
        url = paramUrl;
        final CharSequence colors[] = new CharSequence[]{"NONE", "MALE", "FEMALE"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("SELECT GENDER");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                switch (which) {
                    case 1:
                        if (!url.contains("gender")) {
                            if (url.contains("?")) {
                                if (url.contains("nat"))
                                    url += "&";
                                url += "gender=male";
                            } else
                                url = url + "?gender=male";
                        } else if (url.contains("female"))
                            url = url.replace("female", "male");
                        break;
                    case 2:
                        if (!url.contains("gender")) {
                            if (url.contains("?")) {
                                if (url.contains("nat"))
                                    url += "&";
                                url += "gender=female";
                            } else
                                url += "?gender=female";
                        } else if (url.contains("=male"))
                            url = url.replace("=male", "=female");
                        break;
                    default:

                        if (url.contains("gender")) {
                            if (url.contains("?")) {
                                if (url.contains("nat"))
                                    url = url.replace("&", "");
                                else
                                    url = url.replace("?", "");
                            }
                            if (url.contains("gender=female"))
                                url = url.replace("gender=female", "");
                            else if (url.contains("gender=male"))
                                url = url.replace("gender=male", "");
                        }
                        break;
                }
            }
        });
        builder.show();
        return url;
    }
}
