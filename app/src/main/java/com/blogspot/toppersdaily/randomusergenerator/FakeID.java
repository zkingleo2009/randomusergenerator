package com.blogspot.toppersdaily.randomusergenerator;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.orm.SugarRecord;

import java.io.ByteArrayOutputStream;

/**
 * Created by zaina on 5/11/2016.
 */
public class FakeID extends SugarRecord {
    byte[] Image;
    String FullName;
    String Gender;
    String Street;
    String City;
    String State;
    String ZipCode;
    String Email;
    String Username;
    String Password;
    String DateOfBirth;
    String PhoneNumber;
    String CellNumber;
    String SocialSecurityNumber;
    String Nationality;

    public FakeID() {
    }

    public FakeID(Drawable image,
                  String fullName,
                  String gender,
                  String street,
                  String city,
                  String state,
                  String zipCode,
                  String email,
                  String username,
                  String password,
                  String dateOfBirth,
                  String phoneNumber,
                  String cellNumber,
                  String socialSecurityNumber,
                  String nationality) {

        Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        Image = stream.toByteArray();

        FullName = fullName;
        Gender = gender;
        Street = street;
        City = city;
        State = state;
        ZipCode = zipCode;
        Email = email;
        Username = username;
        Password = password;
        DateOfBirth = dateOfBirth;
        PhoneNumber = phoneNumber;
        CellNumber = cellNumber;
        SocialSecurityNumber = socialSecurityNumber;
        Nationality = nationality;
    }

    public void setImage(Drawable image) {
        Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        Image = stream.toByteArray();

    }

    public FakeID(String fullName,
                  String gender,
                  String street,
                  String city,
                  String state,
                  String zipCode,
                  String email,
                  String username,
                  String password,
                  String dateOfBirth,
                  String phoneNumber,
                  String cellNumber,
                  String socialSecurityNumber,
                  String nationality) {
        FullName = fullName;
        Gender = gender;
        Street = street;
        City = city;
        State = state;
        ZipCode = zipCode;
        Email = email;
        Username = username;
        Password = password;
        DateOfBirth = dateOfBirth;
        PhoneNumber = phoneNumber;
        CellNumber = cellNumber;
        SocialSecurityNumber = socialSecurityNumber;
        Nationality = nationality;
    }

    public String getAddress() {
        return Street + ", " + City + ", " + State + ", " + Nationality + " - " + ZipCode;
    }
}
