package com.blogspot.toppersdaily.randomusergenerator;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    String url = "https://randomuser.me/api/1.0/";
    //    String url = "http://api.randomuser.me/1.0/";
    FakeID fakeID;
    Button saveButton;

    public void BtnListFromDBClicked(View V) {
        Intent intent = new Intent(this, LoadSavedInformation.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                MenuItemsSelected._Selected.AboutSelected();
                break;
            case 0:
            case R.id.gender:
                url = MenuItemsSelected._Selected.GenderSelected(url);
                break;
            case 1:
            case R.id.country:
                url = MenuItemsSelected._Selected.CountrySelected(url);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saveButton = (Button) findViewById(R.id.saveBtn);
        saveButton.setEnabled(false);

        PersonInformation.Image = (ImageView) findViewById(R.id.UserImage);
        PersonInformation.FullName = (TextView) findViewById(R.id.fullName);
        PersonInformation.Gender = (TextView) findViewById(R.id.gender);
        PersonInformation.Address = (TextView) findViewById(R.id.Address);
        PersonInformation.Email = (TextView) findViewById(R.id.email);
        PersonInformation.Username = (TextView) findViewById(R.id.username);
        PersonInformation.Password = (TextView) findViewById(R.id.password);
        PersonInformation.DateOfBirth = (TextView) findViewById(R.id.DateOfBirth);
        PersonInformation.PhoneNumber = (TextView) findViewById(R.id.phoneNumber);
        PersonInformation.CellNumber = (TextView) findViewById(R.id.cellNumber);
        PersonInformation.SocialSecurityNumber = (TextView) findViewById(R.id.socialSecurityNumber);
        PersonInformation.SaveInformation = (Button) findViewById(R.id.saveBtn);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        MenuItemsSelected._Selected.setContext(this);
        MenuItemsSelected._Selected.setUrl(url);

//        LoadTitles();
//        SetClipboard();
    }

    public void saveButtonClicked(View V) {
        fakeID.save();
        Toast.makeText(getApplicationContext(), "Saved Successfully", Toast.LENGTH_LONG).show();
        saveButton.setEnabled(false);
    }

    public void generateRandom(View v) {
        new RetrieveFeedTask().execute(url);
    }

    String nationality = "";
    String thumbnail = "";

    class RetrieveFeedTask extends AsyncTask<String, Void, String> {

        private Exception exception;
        private String jsonData = "";

        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                url.openConnection();
                jsonData = GetHTTPData(urls[0]);

                return jsonData;
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }

        protected void onPostExecute(String data) {
            // TODO: check this.exception
            // TODO: do something with the feed
            if (data != null) {
                try {
                    JSONObject reader = new JSONObject(data);
                    JSONArray json = reader.getJSONArray("results");
                    JSONObject user = json.getJSONObject(0);
                    JSONObject name = user.getJSONObject("name");
                    JSONObject location = user.getJSONObject("location");
                    JSONObject login = user.getJSONObject("login");
                    JSONObject id = user.getJSONObject("id");
                    JSONObject picture = user.getJSONObject("picture");

                    String title = name.getString("title");
                    String first = name.getString("first");
                    String last = name.getString("last");

                    String gender = user.getString("gender");

                    String street = location.getString("street");
                    String city = location.getString("city");
                    String state = location.getString("state");
                    String zip = location.getString("postcode");

                    String email = user.getString("email");
                    String username = login.getString("username");
                    String password = login.getString("password");
                    String SSN = id.getString("value");
                    long dob = user.getLong("dob");
                    String phone = user.getString("phone");
                    String cell = user.getString("cell");
                    nationality = user.getString("nat");
                    thumbnail = picture.getString("large");


                    String eadresses[] = {"mail.com", "mail2web.com", "aol.com", "excite.com", "icqmail.com",
                            "aol.com", "byke.com", "postmaster.co.uk", "gmail.com", "lycos.com", "walla.com",
                            "hotmail.co.uk", "facebook.com", "rocketmail.com", "fastmail.fm", "netaddress.com",
                            "yahoo.com", "ymail.com", "zohomail.com", "hotmail.com", "live.com", "hushmail.com"};
                    int alu = new Random().nextInt(eadresses.length);
                    email = email.replace("example.com", eadresses[alu]);
                    String[] s = state.split(" ");
                    state = "";
                    for (int i = 0; i < s.length; i++) {
                        s[i].replace(title.charAt(0), Character.toUpperCase(title.charAt(0)));
                        state = state + s[i] + " ";
                    }
                    String date = new java.text.SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date(dob * 1000));
                    String[] array;
                    if (SSN == "null") {
                        SSN = "N/A";
                    }
                    array = new String[]{title.replace(title.charAt(0), Character.toUpperCase(title.charAt(0))) +
                            " " + first.replace(first.charAt(0), Character.toUpperCase(first.charAt(0))) +
                            " " + last.replace(last.charAt(0), Character.toUpperCase(last.charAt(0))),
                            "" + gender.replace(gender.charAt(0), Character.toUpperCase(gender.charAt(0))),
                            "" + street,
                            "" + city.replace(city.charAt(0), Character.toUpperCase(city.charAt(0))),
                            "" + state,
                            "" + zip,
                            "" + email,
                            "" + username,
                            "" + password,
                            "" + date,
                            "" + phone,
                            "" + cell,
                            "" + SSN,
                            nationality};

                    new getCountry(array).execute(nationality);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public String GetHTTPData(String urlString) {
            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                // Check the connection status
                if (urlConnection.getResponseCode() == 200) {
                    // if response code = 200 ok
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    // Read the BufferedInputStream
                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        sb.append(line);
                    }
                    jsonData = sb.toString();
                    // End reading...............

                    // Disconnect the HttpURLConnection
                    urlConnection.disconnect();
                } else {
                    // Do something
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }
            // Return the data from specified url
            return jsonData;
        }

    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            PersonInformation.Image.setImageBitmap(result);
            fakeID.setImage(bmImage.getDrawable());
            saveButton.setEnabled(true);
        }
    }


    private class getCountry extends AsyncTask<String, Void, String> {
        String code = "";
        String urlAddr = "";
        String[] array;

        public getCountry(String[] array) {
            code = nationality;
            this.array = array;
            this.urlAddr = "https://restcountries.eu/rest/v1/alpha/" + code;
        }

        String jsonData = "";

        protected String doInBackground(String... urls) {
            String country = "";
            try {
                URL url = new URL(urlAddr);
                url.openConnection();
                jsonData = GetHTTPData(urlAddr);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return jsonData;
        }

        public String GetHTTPData(String urlString) {
            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                // Check the connection status
                if (urlConnection.getResponseCode() == 200) {
                    // if response code = 200 ok
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    // Read the BufferedInputStream
                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        sb.append(line);
                    }
                    jsonData = sb.toString();
                    // End reading...............

                    // Disconnect the HttpURLConnection
                    urlConnection.disconnect();
                } else {
                    // Do something
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }
            // Return the data from specified url
            return jsonData;
        }

        protected void onPostExecute(String result) {

            JSONObject reader = null;
            try {
                reader = new JSONObject(result);
                String country = reader.getString("name");
                nationality = country;
                array[array.length - 1] = nationality;

                new DownloadImageTask((ImageView) findViewById(R.id.UserImage))
                        .execute(thumbnail);

                fakeID = new FakeID(array[0],
                        array[1],
                        array[2],
                        array[3],
                        array[4],
                        array[5],
                        array[6],
                        array[7],
                        array[8],
                        array[9],
                        array[10],
                        array[11],
                        array[12],
                        array[13]);
                PersonInformation.FullName.setText("Full Name: " + array[0]);
                PersonInformation.Gender.setText("Gender: " + array[1]);
                PersonInformation.Address.setText("Address: " + fakeID.getAddress());
                PersonInformation.Email.setText("Email: " + array[6]);
                PersonInformation.Username.setText("Username: " + array[7]);
                PersonInformation.Password.setText("Password: " + array[8]);
                PersonInformation.DateOfBirth.setText("Date of Birth: " + array[9]);
                PersonInformation.PhoneNumber.setText("Phone Number: " + array[10]);
                PersonInformation.CellNumber.setText("Cell Number: " + array[11]);
                PersonInformation.SocialSecurityNumber.setText("SSN: " + array[12]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
