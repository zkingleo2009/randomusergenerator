package com.blogspot.toppersdaily.randomusergenerator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by zaina on 4/30/2016.
 */
public class CustomAdapter extends BaseAdapter implements View.OnClickListener {
    LoadSavedInformation context;
    List<FakeID> Person;
    FakeID fakeID = null;
    private static LayoutInflater inflater = null;

    public CustomAdapter(LoadSavedInformation mainActivity, List<FakeID> fakeIDs) {
        context = mainActivity;
        Person = fakeIDs;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        if (Person.size() <= 0)
            return 1;
        return Person.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }


    public class Holder {
        TextView FullName;
        TextView Gender;
        TextView Address;
        TextView Email;
        TextView Username;
        TextView Password;
        TextView DateOfBirth;
        TextView PhoneNumber;
        TextView CellNumber;
        TextView SocialSecurityNumber;
        ImageView Image;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        View vi = convertView;
        View rowView = convertView;
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            rowView = inflater.inflate(R.layout.person_information_card, null);
            holder.Image = (ImageView) rowView.findViewById(R.id.customUserImage);
            holder.FullName = (TextView) rowView.findViewById(R.id.customFullName);
            holder.Gender = (TextView) rowView.findViewById(R.id.customGender);
            holder.Address = (TextView) rowView.findViewById(R.id.customAddress);
            holder.Email = (TextView) rowView.findViewById(R.id.customEmail);
            holder.Username = (TextView) rowView.findViewById(R.id.customUsername);
            holder.Password = (TextView) rowView.findViewById(R.id.customPassword);
            holder.DateOfBirth = (TextView) rowView.findViewById(R.id.customDateOfBirth);
            holder.PhoneNumber = (TextView) rowView.findViewById(R.id.customPhoneNumber);
            holder.CellNumber = (TextView) rowView.findViewById(R.id.customCellNumber);
            holder.SocialSecurityNumber = (TextView) rowView.findViewById(R.id.customSocialSecurityNumber);
            rowView.setTag(holder);
        } else
            holder = (Holder) rowView.getTag();
        if (Person.size() <= 0) {
            Toast.makeText(context, "No Data Saved", Toast.LENGTH_LONG).show();
        } else {
            fakeID = null;
            fakeID = Person.get(position);
            Bitmap bitmap = BitmapFactory.decodeByteArray(fakeID.Image, 0, Person.get(position).Image.length);
            holder.Image.setImageBitmap(bitmap);
            holder.FullName.setText("Full Name: " + fakeID.FullName);
            holder.Gender.setText("Gender: "+fakeID.Gender);
            holder.Address.setText("Address: "+fakeID.getAddress());
            holder.Email.setText("Email: "+fakeID.Email);
            holder.Username.setText("Username: "+fakeID.Username);
            holder.Password.setText("Password: "+fakeID.Password);
            holder.DateOfBirth.setText("Date of Birth: "+fakeID.DateOfBirth);
            holder.PhoneNumber.setText("Phone Number: "+fakeID.PhoneNumber);
            holder.CellNumber.setText("Cell Number: "+fakeID.CellNumber);
            holder.SocialSecurityNumber.setText("SSN: "+fakeID.SocialSecurityNumber);
            rowView.setOnClickListener(new OnItemClickListener(position));
        }
        return rowView;
    }

    private class OnItemClickListener implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
//            CustomListViewAndroidExample sct = (CustomListViewAndroidExample) activity;
            /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/
//            sct.onItemClick(mPosition);
        }
    }
}
