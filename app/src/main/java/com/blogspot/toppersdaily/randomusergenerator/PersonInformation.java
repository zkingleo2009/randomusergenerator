package com.blogspot.toppersdaily.randomusergenerator;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by zaina on 5/11/2016.
 */
public class PersonInformation {
    public static ImageView Image;
    public static TextView FullName;
    public static TextView Gender;
    public static TextView Address;
    public static TextView Email;
    public static TextView Username;
    public static TextView Password;
    public static TextView DateOfBirth;
    public static TextView PhoneNumber;
    public static TextView CellNumber;
    public static TextView SocialSecurityNumber;
    public static Button SaveInformation;
}
