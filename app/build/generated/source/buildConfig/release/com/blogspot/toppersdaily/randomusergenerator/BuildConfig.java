/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.blogspot.toppersdaily.randomusergenerator;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.blogspot.toppersdaily.randomusergenerator";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 7;
  public static final String VERSION_NAME = "2.0";
}
